﻿namespace ListTasks
{
    public class ListTasks
    {
        public static void Main()
        {

        }

        public static bool EraseMiddleElementInLinkedList(LinkedList<int> input)
        {
            if (input.Count == 0)
            {
                return false;
            }

            if (input.Count == 1)
            {
                input.RemoveFirst();

                return true;
            }

            var middleCount = (int)Math.Floor((decimal)input.Count / 2);
            var first = 0;

            for (int i = 0; i < middleCount; i++)
            {
                first = input.First();

                input.RemoveFirst();

                input.AddLast(first);
            }

            input.RemoveFirst();

            if (input.Count % 2 != 0)
            {
                middleCount--;
            }

            for (int i = 0; i < middleCount; i++)
            {
                first = input.First();

                input.RemoveFirst();

                input.AddLast(first);
            }

            return true;
        }

        public static List<int> GetUniqueElements(List<int> list)
        {
            var visited = new HashSet<int>();

            list.ForEach(x => visited.Add(x));

            return visited.ToList();
        }
    }
}